import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Main {

    public static void main(String[] args) {
        Class c;
        String classArgs = args[0];
        try {
            c = Class.forName(classArgs);
            getClassInterface(c);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static void getClassInterface(Class c){
        String methodsName = "";
        String className = "";
        String classFieldsName = "";
        Method[] methods = c.getDeclaredMethods();
        Field[] fields = c.getDeclaredFields();
        Class[] interfaces = c.getInterfaces();

        //classname
        className += Modifier.toString(c.getModifiers())+ " class " + c.getName();

        //superclass
        if(c.getSuperclass()!=null){
            className += " extends " + c.getSuperclass().getName() + " ";
        }

        //interface
        if(interfaces.length>0){
            className += "implements ";
            for(int i=0; i<interfaces.length; i++){
                className += interfaces[i].getName();
                if(i<interfaces.length-1){
                    className += ", ";
                }
            }
        }

        className += "{";

        //fields
        for(int i=0; i<fields.length; i++){
            classFieldsName += Modifier.toString(fields[i].getModifiers()) + " " + fields[i].getType().getName() + " " + fields[i].getName() + ";";
        }

        //methods
        for(int i=0; i<methods.length; i++){
            methodsName += Modifier.toString(c.getModifiers()) + " " + methods[i].getReturnType().getName()+ " " + methods[i].getName() + "(";
            for(int j = 0; j<methods[i].getParameterCount(); j++){
                methodsName += methods[i].getGenericParameterTypes()[j].getTypeName() + " " + methods[i].getParameters()[j].getName();
                if(j<methods[i].getParameterCount()-1) {
                    methodsName += ", ";
                }
            }
            methodsName += ")";

            if(methods[i].getGenericExceptionTypes().length>0){
                methodsName += " throws ";
                for(int j=0; j<methods[i].getGenericExceptionTypes().length; j++) {
                    methodsName += methods[i].getGenericExceptionTypes()[j].getTypeName();
                    if(j<methods[i].getGenericExceptionTypes().length-1){
                        methodsName += ", ";
                    }
                }
            }


            methodsName += ";\n";
        }
        methodsName += "}";

        //print strings
        System.out.println(className);
        System.out.println(classFieldsName);
        System.out.println(methodsName);

        if(c.getSuperclass()!=null){
            getClassInterface(c.getSuperclass());
        }

    }
}
