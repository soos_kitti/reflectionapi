/**
 * Abstract class for testing, will be extended by TestClass
 */
public abstract class TestAbstract {
    /**
     * Name (String) field
     */
    private final String name;

    /**
     * Constructor for the class
     * @param name The name (String) that will be passed to the @name field
     */
    public TestAbstract(String name){
        this.name = name;
    }

    /**
     * Overrides the @Object.java toString method to return the @name field instead
     * @return returns the name via toString
     */
    @Override
    public String toString(){
        return this.name;
    }

    /**
     * Deprecated method
     */
    @Deprecated
    public void deprecatedMethod(){
    }
}