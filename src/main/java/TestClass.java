import java.math.BigInteger;

/**
 * Test class
 */
public class TestClass extends TestAbstract implements TestInterface{
    /**
     * Test int field
     */
    private int testField;

    /**
     * Getter for test field (int), gives autoboxed Integer for more flexible use
     * @return returns the test field as a wrapped Integer
     */
    public Integer getTestField(){
        return testField;
    }

    /**
     * Sets the test field (Integer)
     * @param value The value that will be assigned to the test field
     */
    public void setTestField(Integer value){
        this.testField = value;
    }

    /**
     * Sets the test field (int)
     * @param value The value that will be assigned to the test field
     */
    private void setTestField(int value){
        this.testField = value;
    }

    /**
     * Constructor for the test class
     * @param testField Test field (int) value that will be given to the appropriate field
     * @param name Name (String) that will be used via @TestAbstract toString override
     */
    public TestClass(int testField, String name){
        // Passing name to parent
        super(name);
        // Setting test field via constructor
        this.setTestField(testField);
    }

    /**
     * Implementing interface @TestInterface
     * @param mathThing A BigInteger for testing import
     * @throws Exception always
     */
    @Override
    public void throwError(BigInteger mathThing) throws Exception {
        throw new Exception("Throw error method called.");
    }
}
