import java.math.BigInteger;

/**
 * Interface for testing purposes, will be implemented by @TestClass
 */
public interface TestInterface {
    void throwError(BigInteger mathThing) throws Exception;
}